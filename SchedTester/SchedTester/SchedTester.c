// SchedTester.cpp
#include "SchedTester.h"

void RoundRobin(struct summary *summary)
{
    START_ALGO("Round Robin");

    struct proc* p;
    for (;;)
    {
        int executed = 0;
        for (p = proc; p < &proc[NPROC]; p++)
            if (RUNNABLE(p))
            {
                executed = 1;
                SWTCH(p, &time, summary);
            }

        if (executed == 0) break;
    }

    END_ALGO();
}

void PriorityRR(struct summary* summary)
{
  START_ALGO("Priority (Round Robin Mode)");

  for (;;)
  {
    struct proc* p;
    struct proc* p1;
    struct proc* chosenProcess = 0;

    int executed = 0;
    for (p = proc; p < &proc[NPROC]; p++) {
      if (RUNNABLE(p)) {
        chosenProcess = p;
        for (p1 = proc; p1 < &proc[NPROC]; p1++) {
          if (p1 == p) continue;

          if (RUNNABLE(p1))
          {
            if (chosenProcess->priority > p1->priority)
              chosenProcess = p1;
          }
        }

        SWTCH(chosenProcess, &time, summary);
        executed = 1;
      }
    }

    if (executed == 0) break;
  }

  END_ALGO();
}

void PriorityFCFS(struct summary* summary)
{
  START_ALGO("Priority (First Come First Serve Mode)");

  for (;;)
  {
    struct proc* p;
    struct proc* chosenProcess = 0;

    int availiable = 0;
    for (p = proc; p < &proc[NPROC]; p++) {
      if (RUNNABLE(p))
      {
        availiable = 1;
        if (chosenProcess == 0 || p->priority < chosenProcess->priority)
          chosenProcess = p;
      }
    }

    if (availiable == 0) break;

    if (chosenProcess != 0) {
      while (RUNNABLE(chosenProcess))
        SWTCH(chosenProcess, &time, summary);
    }
    else
      time++;
  }

  END_ALGO();
}

void ShortestJobFirst(struct summary* summary)
{
  START_ALGO("Shortest Job First");

  struct proc* p;

  for (;;)
  {
    int availiable = 0;
    struct proc* chosenProcess = 0;
    for (p = proc; p < &proc[NPROC]; p++) {
      if (RUNNABLE(p))
      {
        availiable = 1;
        if (chosenProcess == 0 || p->burstTime < chosenProcess->burstTime)
          chosenProcess = p;
      }
    }
    
    if (availiable == 0) break;

    if (chosenProcess != 0) {
      while (chosenProcess->executedTime < chosenProcess->burstTime)
        SWTCH(chosenProcess, &time, summary);
    }
    else
      time++;
  }

  END_ALGO();
}

void MultilevelQueue(struct summary* summary)
{
  START_ALGO("Multilevel Queue");

  int queuecount = sizeof(SCHED_QUEUES) / sizeof(SCHED_QUEUES[0]);

  for (;;)
  {
    struct proc* pp;
    int runnableProcesses = 0;
    int activeProcesses = 0;
    for (pp = proc; pp < &proc[NPROC]; pp++) {
      if (RUNNABLE(pp)) runnableProcesses = 1;
      if (pp->executedTime < pp->burstTime) activeProcesses = 1;
    }
    if (activeProcesses == 0) break;
    if (runnableProcesses == 0)
    {
      time++;
      continue;
    }

    for (int i = 0; i < queuecount; i++)
    {
      queue_info_t queue = SCHED_QUEUES[i];

      if (queue.currentproc == 0)
        queue.currentproc = proc;

      if (queue.algo == 0) // ROUND ROBIN
      {
        struct proc* p;
        int executed = 0;
        for (p = queue.currentproc; p < &proc[NPROC]; p++) {
          if (p->currentQueue == i && RUNNABLE(p)) {
            SWTCH(p, &time, summary);
            if (i < queuecount - 1) p->currentQueue = i + 1;
            executed++;
          }

          queue.currentproc = p + 1;
          if (executed >= queue.cputime) break;
        }
      }
      else if (queue.algo == 1) // Priority RR
      {
        struct proc* p;
        struct proc* p1;
        struct proc* chosenProcess = 0;
        int executed = 0;
        for (p = queue.currentproc; p < &proc[NPROC]; p++) {
          if (p->currentQueue == i && RUNNABLE(p)) {
            chosenProcess = p;
            for (p1 = proc; p1 < &proc[NPROC]; p1++) {
              if (p1 == p) continue;

              if (p->currentQueue == i && RUNNABLE(p1))
              {
                if (chosenProcess->priority > p1->priority)
                  chosenProcess = p1;
              }
            }

            SWTCH(chosenProcess, &time, summary);
            if (i < queuecount - 1) p->currentQueue = i + 1;

            executed++;
          }

          queue.currentproc = p + 1;
          if (executed >= queue.cputime) break;
        }
      }
      else if (queue.algo == 2) // Priority FCFS
      {
        struct proc* p;
        struct proc* chosenProcess = 0;
        int executed = 0;
        for (p = proc; p < &proc[NPROC]; p++) {
          if (p->currentQueue == i && RUNNABLE(p))
          {
            if (chosenProcess == 0 || p->priority < chosenProcess->priority)
              chosenProcess = p;
          }
        }

        if (chosenProcess != 0) {
          while (RUNNABLE(chosenProcess))
          {
            SWTCH(chosenProcess, &time, summary);
            executed++;
          }

          if (i < queuecount - 1) p->currentQueue = i + 1;
          queue.currentproc = chosenProcess + 1;
          if (executed >= queue.cputime) break;
        }
      }
      else if (queue.algo == 3) // SJF
      {
        struct proc* p;
        struct proc* chosenProcess = 0;
        int executed = 0;
        for (p = proc; p < &proc[NPROC]; p++) {
          if (p->currentQueue == i && RUNNABLE(p))
          {
            if (chosenProcess == 0 || p->burstTime < chosenProcess->burstTime)
              chosenProcess = p;
          }
        }

        if (chosenProcess != 0) {
          while (chosenProcess->executedTime < chosenProcess->burstTime)
          {
            SWTCH(chosenProcess, &time, summary);
            executed++;
          }

          if (i < queuecount - 1) p->currentQueue = i + 1;
          queue.currentproc = chosenProcess + 1;
          if (executed >= queue.cputime) break;
        }
      }
      else
        printf("scheduler: invalid queue algorithm");
    }
  }

  END_ALGO();
}

int main()
{
    struct summary summary;

    RoundRobin(&summary);
    printSummary(&summary);
    printf("\n");

    PriorityRR(&summary);
    printSummary(&summary);
    printf("\n");

    PriorityFCFS(&summary);
    printSummary(&summary);
    printf("\n");

    ShortestJobFirst(&summary);
    printSummary(&summary);
    printf("\n");

    MultilevelQueue(&summary);
    printSummary(&summary);
}

