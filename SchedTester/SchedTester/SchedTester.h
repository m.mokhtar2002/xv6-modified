#pragma once

#include <stdio.h>

struct proc {
  int arrivalTime;
  int burstTime;
  int priority;

  // Used during runtime
  int executedTime;
  int currentQueue;
};

struct summary {
  char* algorithm;
  float avgResponse;
  float avgTurnaround;
  float avgWaiting;
};

const struct proc PROCESS_LIST[] =
{
    {0, 3, 1},
    {0, 3, 2},
    {2, 2, 0}
};

#define START_ALGO(name)    summary->algorithm = name; \
                            summary->avgResponse = 0; \
                            summary->avgTurnaround = 0; \
                            summary->avgWaiting = 0; \
                            struct proc* proc = malloc(sizeof(PROCESS_LIST)); \
                            memcpy(proc, &PROCESS_LIST, sizeof(PROCESS_LIST)); \
                            int time = 0;

#define END_ALGO()          free(proc); \
                            summary->avgResponse /= NPROC; \
                            summary->avgTurnaround /= NPROC; \
                            summary->avgWaiting /= NPROC; \

#define NPROC               sizeof(PROCESS_LIST)/sizeof(struct proc)

#define RUNNABLE(p)         p->executedTime < p->burstTime && time >= p->arrivalTime

void SWTCH(struct proc* p, int* time, struct summary* summary)
{
  if (p->executedTime == 0)
    summary->avgResponse += *time - p->arrivalTime;

  p->executedTime++; (*time)++;

  if (p->executedTime == p->burstTime)
  {
    int turnaround = *time - p->arrivalTime;
    summary->avgTurnaround += turnaround;
    summary->avgWaiting += turnaround - p->burstTime;
  }
}

void printSummary(struct summary* summary)
{
  printf("Stats for \"%s\" algorithm:\n\tAverage Response Time: %f\n\tAverage Waiting Time: %f\n\tAverage Turnaround Time: %f\n",
    summary->algorithm, summary->avgResponse, summary->avgWaiting, summary->avgTurnaround);
}

typedef struct _queue_info_t
{
  int algo; // Same settings as SCHED_ALGO
  int cputime;
  void* currentproc;
} queue_info_t;

static const queue_info_t SCHED_QUEUES[] = {
  { 0, 1 }, // RR, 1 unit of cpu time
  { 0, 1 }, // RR, 2 units of cpu time
  { 1, 3 }, // Priority RR, 2 units of cpu time
};
