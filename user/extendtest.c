#include "kernel/types.h"
#include "kernel/fcntl.h"
#include "user/user.h"

#define N 100

struct test {
  char name;
  int number;
};

void
save(void)
{
  int fd;
  struct test t;
  t.name = 's';
  t.number = 1;

  fd = open("backup", O_CREATE | O_RDWR);
  if (fd >= 0) {
    printf("ok: create backup file succeed\n");
  }
  else {
    printf("error: create backup file failed\n");
    exit(3);
  }

  int size = sizeof(t);

  if (write(fd, &t, size) != size) {
    printf("error: write to backup file failed\n");
    exit(4);
  }
  printf("new file size: %d\n", size);
  close(fd);
}

void
extendfile(void)
{
  int fd;

  fd = open("backup", O_RDWR);
  if (fd >= 0) {
    printf("ok: opening backup file succeed\n");
  }
  else {
    printf("error: opening backup file failed\n");
    exit(3);
  }

  int original = 8;
  int size = 100;
  if (extend(fd, original, size) != size) {
    printf("error: extending file failed\n");
    exit(4);
  }

  printf("extending file by %d\n", size);
  close(fd);
}

void
load(void)
{
  int fd;
  struct test t;

  fd = open("backup", O_RDONLY);
  if (fd >= 0) {
    printf("ok: read backup file succeed\n");
  }
  else {
    printf("error: read backup file failed\n");
    exit(2);
  }

  int size = sizeof(t);
  if (read(fd, &t, size) != size) {
    printf("error: read from backup file failed\n");
    exit(1);
  }
  printf("file contents name %c and number %d", t.name, t.number);
  printf("read ok\n");
  close(fd);
}

int
main(void)
{
  save();
  load();
  extendfile();
  load();

  exit(0);
}
