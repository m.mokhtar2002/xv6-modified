#define NPROC        64  // maximum number of processes
#define NCPU          8  // maximum number of CPUs
#define NOFILE       16  // open files per process
#define NFILE       100  // open files per system
#define NINODE       50  // maximum number of active i-nodes
#define NDEV         10  // maximum major device number
#define ROOTDEV       1  // device number of file system root disk
#define MAXARG       32  // max exec arguments
#define MAXOPBLOCKS  10  // max # of blocks any FS op writes
#define LOGSIZE      (MAXOPBLOCKS*3)  // max data blocks in on-disk log
#define NBUF         (MAXOPBLOCKS*3)  // size of disk block cache
#define FSSIZE       1024*32  // size of file system in blocks
#define MAXPATH      128   // maximum file path name
#define BLOCK_SIZE   1024  // Size of one disk block
#define PAGE_SIZE    4096  // Page size 
#define SCHED_ALGO   0  // 0: RoundRobin, 1: PriorityRR, 2: PriorityFCFS, 3: SJF, 4: Multilevel Queue

#if SCHED_ALGO == 3 || SCHED_ALGO == 4 
#define SJF_AVERAGING_VALUE 0.5f
#endif
#if SCHED_ALGO == 4
typedef struct _queue_info_t
{
  int algo; // Same settings as SCHED_ALGO
  int cputime;
  void* currentproc;
} queue_info_t;

static const queue_info_t SCHED_QUEUES[] = {
  { 0, 1, 0 }, // RR, 1 unit of cpu time
  { 0, 2, 0 }, // RR, 2 units of cpu time
  { 1, 2, 0 }, // Priority RR, 2 units of cpu time
};
#endif

#define ENABLE_SWAPPING 0
#if ENABLE_SWAPPING == 1
#define MAXPAGES     16  // The maximum number of pages per process
#define LRU_SWAPPING 1  // 0: FCFS, 1: LRU
#endif
